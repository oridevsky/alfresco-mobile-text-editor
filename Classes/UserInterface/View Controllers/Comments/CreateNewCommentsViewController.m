//
//  CreateNewCommentsViewController.m
//  FreshDocs
//
//  Created by oleg sfd on 29.05.12.
//  Copyright (c) 2012 . All rights reserved.
//

#import "CreateNewCommentsViewController.h"
#import "Utility.h"
#import "NodeRef.h"
#import "CommentsHttpRequest.h"

@implementation CreateNewCommentsViewController
@synthesize timer;
@synthesize HUD;
@synthesize cmisObjectId;
@synthesize selectedAccountUUID;
@synthesize tenantID;
@synthesize commentsRequest;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];    
	// Do any additional setup after loading the view, typically from a nib.
    // Load in the index file
    if (IS_IPAD) {
        webView.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y + 50, webView.frame.size.width, webView.frame.size.height - 50);
    } else {
        webView.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y + 30, webView.frame.size.width, webView.frame.size.height - 30);
    }
    webView.scrollView.showsHorizontalScrollIndicator = NO;
    webView.backgroundColor = [UIColor whiteColor];
    for (UIView* subView in [webView subviews])
    {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            for (UIView* shadowView in [subView subviews])
            {
                if ([shadowView isKindOfClass:[UIImageView class]]) {
                    [shadowView setHidden:YES];
                }
            }
        }
    }
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *indexFileURL = [bundle URLForResource:@"text" withExtension:@"html"];
    [webView loadRequest:[NSURLRequest requestWithURL:indexFileURL]];
    webView.delegate = self;
    
    [toolBar addSubview:buttonsView];
    
    // Add ourselves as observer for the keyboard will show notification so we can remove the toolbar
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];   
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuDidAppear) name:UIMenuControllerDidShowMenuNotification object:nil];  
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuDidHide) name:UIMenuControllerDidHideMenuNotification object:nil];
   
    
    // Set up navbar items now
    [self checkSelection:self];
    
    // Set timer to checkSelection every 0.1 seconds
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(checkSelection:) userInfo:nil repeats:YES];
    
    //actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a font color" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Blue", @"Yellow", @"Green", @"Red", @"Orange", nil];
    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a font color" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"", @"", @"", nil];
    //actionSheet.frame = CGRectMake(0, 0, 200, 200); 
    if (IS_IPAD){
        selectColorView.frame = CGRectMake(0, 0, 276, 176);
    }
    [actionSheet addSubview:selectColorView];
    actionSheetShown =  NO;
    
    UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(onSaveButton:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    [saveButton release];
    
    
    [[UIMenuController sharedMenuController] setTargetRect:CGRectMake(0,100,320,200) inView:webView];    
        
}

- (void)checkSelection:(id)sender {
    BOOL boldEnabled = [[webView stringByEvaluatingJavaScriptFromString:@"document.queryCommandState('Bold')"] boolValue];
    BOOL italicEnabled = [[webView stringByEvaluatingJavaScriptFromString:@"document.queryCommandState('Italic')"] boolValue];
    BOOL undoAvailable = [[webView stringByEvaluatingJavaScriptFromString:@"document.queryCommandEnabled('undo')"] boolValue];
    BOOL redoAvailable = [[webView stringByEvaluatingJavaScriptFromString:@"document.queryCommandEnabled('redo')"] boolValue];
    
    if (boldEnabled) {
        if (boldButtonState != ToolbarButtonStateOn) {
            [boldButton setBackgroundImage:[UIImage imageNamed:@"TextEditorBoldS.png"] forState:UIControlStateNormal];
            boldButtonState = ToolbarButtonStateOn;
        }
    } else {
        if (boldButtonState != ToolbarButtonStateOff) {
            [boldButton setBackgroundImage:[UIImage imageNamed:@"TextEditorBold.png"] forState:UIControlStateNormal];
            boldButtonState = ToolbarButtonStateOff;
        }
    }
    
    if (italicEnabled) {
        if (italicButtonState != ToolbarButtonStateOn) {
            [italicButton setBackgroundImage:[UIImage imageNamed:@"TextEditorItalicS.png"] forState:UIControlStateNormal];
            italicButtonState = ToolbarButtonStateOn;
        }
    } else {
        if (italicButtonState != ToolbarButtonStateOff) {
            [italicButton setBackgroundImage:[UIImage imageNamed:@"TextEditorItalic.png"] forState:UIControlStateNormal];
            italicButtonState = ToolbarButtonStateOff;
        }
    }   
    
    if (undoAvailable) {
        if (undoButtonState != ToolbarButtonStateOn) {
            [undoButton setBackgroundImage:[UIImage imageNamed:@"TextEditorUndoA.png"] forState:UIControlStateNormal];
            undoButtonState = ToolbarButtonStateOn;
        }
    } else {
        if (undoButtonState != ToolbarButtonStateOff) {
            [undoButton setBackgroundImage:[UIImage imageNamed:@"TextEditorUndo.png"] forState:UIControlStateNormal];
            undoButtonState = ToolbarButtonStateOff;
        }
    }
    
    if (redoAvailable) {
        if (redoButtonState != ToolbarButtonStateOn) {
            [redoButton setBackgroundImage:[UIImage imageNamed:@"TextEditorRedoA.png"] forState:UIControlStateNormal];
            redoButtonState != ToolbarButtonStateOn;
        }
    } else {
        if (redoButtonState != ToolbarButtonStateOff) {
            [redoButton setBackgroundImage:[UIImage imageNamed:@"TextEditorRedo.png"] forState:UIControlStateNormal];
            redoButtonState != ToolbarButtonStateOff;
        }
    }
    NSString *request = [[NSString alloc] initWithFormat:@"scroll(%d)", IS_IPAD ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? 500: 200) : (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? 100: 16)];
    [webView stringByEvaluatingJavaScriptFromString:request];
    [request release];
}

- (void)viewWillAppear:(BOOL)animated {
    boldButtonState = ToolbarButtonStateUnknown;
    italicButtonState = ToolbarButtonStateUnknown;
    undoButtonState = ToolbarButtonStateUnknown;
    redoButtonState = ToolbarButtonStateUnknown;
    [super viewWillAppear:animated];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)_webView {
    //[_webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('content').focus();"];
    
}

- (BOOL)becomeFirstResponder{
    return YES;
}

- (void)actionSheet:(UIActionSheet *)_actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSString *selectedButtonTitle = [_actionSheet buttonTitleAtIndex:buttonIndex];
    //selectedButtonTitle = [selectedButtonTitle lowercaseString];    
        
}

#pragma mark Removing toolbar

- (void)keyboardWillShow:(NSNotification *)note {
    [self performSelector:@selector(removeBar) withObject:nil afterDelay:0];    
}

/*- (void)setMenu{ 
    CGRect r = [UIMenuController sharedMenuController].menuFrame;    
    if (!IS_IPAD ){        
        if (r.origin.y < 110){
            //needReShow = NO;
            [[UIMenuController sharedMenuController] setArrowDirection:UIMenuControllerArrowUp];        
            [[UIMenuController sharedMenuController] setTargetRect:CGRectMake(r.origin.x, r.origin.y - 130, r.size.width, r.size.height) inView:webView];            
        }
    }    
    
}

- (void)menuDidAppear{
    //CGRect r = [UIMenuController sharedMenuController].menuFrame;
    //NSLog(@"%f",r.origin.y);
    
    [self performSelector:@selector(setMenu) withObject:nil afterDelay:0.01];
        
}*/

- (void)removeBar {
    // Locate non-UIWindow.
    UIWindow *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]) {
        if (![[testWindow class] isEqual:[UIWindow class]]) {
            keyboardWindow = testWindow;
            break;
        }
    }
    
    // Locate UIWebFormView.
    for (UIView *possibleFormView in [keyboardWindow subviews]) {       
        // iOS 5 sticks the UIWebFormView inside a UIPeripheralHostView.
        if ([[possibleFormView description] rangeOfString:@"UIPeripheralHostView"].location != NSNotFound) {
            for (UIView *subviewWhichIsPossibleFormView in [possibleFormView subviews]) {
                if ([[subviewWhichIsPossibleFormView description] rangeOfString:@"UIWebFormAccessory"].location != NSNotFound) {
                    [subviewWhichIsPossibleFormView removeFromSuperview];
                }
            }
        }
    }
}


- (IBAction)onBold:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand(\"Bold\")"];
}

- (IBAction)onItalic:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand(\"Italic\")"];
}

- (IBAction)onList1:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand('InsertOrderedList')"];
}

- (IBAction)onList2:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand('InsertUnorderedList')"];
}

- (IBAction)onFontColor:(id)sender{
    
    [actionSheet showFromRect:fontColorButton.frame inView:self.view animated:YES];
    actionSheetShown = YES;
    //[actionSheet showInView:self.view];
    //[actionSheet showFromBarButtonItem:(UIBarButtonItem *)sender animated:YES];
}

- (IBAction)onUndo:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand('undo')"];
}

- (IBAction)onRedo:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand('redo')"];
}

- (IBAction)onClearFormat:(id)sender{
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand('RemoveFormat')"];
}

- (IBAction)onSelectColorButton:(id)sender{
    UIButton * b = (UIButton *)sender;
    //NSLog(@"%@",[b.backgroundColor CGColor]);
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.execCommand('foreColor', false, '%@')", [self colorFromTag:b.tag]]];
    
}

#pragma mark - MBProgressHUD Helper Methods

- (void)startHUD
{
	if (!self.HUD)
    {
		self.HUD = createAndShowProgressHUDForView(webView);
	}
}

- (void)stopHUD
{
	if (self.HUD)
    {
        stopProgressHUD(self.HUD);
		self.HUD = nil;
	}
}

#pragma mark - save comment

- (IBAction)onSaveButton:(id)sender{
    NSString * commentString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    //NSLog(@"%@",commentString);
    commentString = [commentString stringByReplacingOccurrencesOfString:@"contenteditable=\"true\"" withString:@"contenteditable=\"false\""];    
    
    [self startHUD];    
    
    self.commentsRequest = [CommentsHttpRequest CommentsHttpPostRequestForNodeRef:[NodeRef nodeRefFromCmisObjectId:self.cmisObjectId] comment: commentString accountUUID:selectedAccountUUID tenantID:self.tenantID];
    
    
    [commentsRequest setDelegate:self];
    [commentsRequest setDidFinishSelector:@selector(commentsHttpRequestDidFinish:)];
    [commentsRequest setDidFailSelector:@selector(commentsHttpRequestDidFail:)];    
    [commentsRequest startAsynchronous];
}

- (void)commentsHttpRequestDidFinish:(id)sender
{
    //NSLog(@"commentsHttpRequestDidFinish");    
    [self stopHUD];
    /*UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Comment sent successfully. Send another comment ?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Yes", nil) otherButtonTitles:NSLocalizedString(@"No", nil), nil];
    alert.tag = 1;
    [alert show];
    [alert release];*/
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)commentsHttpRequestDidFail:(id)sender
{
    //NSLog(@"commentsHttpRequestDidFail!");
    [self stopHUD];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Comment sent with error, try to send again ?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Yes", nil) otherButtonTitles:NSLocalizedString(@"No", nil), nil];
    alert.tag = 2;
    [alert show];
    [alert release];
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1){
        if (buttonIndex == 1){
            //No
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            //Yes
            
        }
    }else{
        if (buttonIndex == 1){
            
        }else{
            [self onSaveButton:nil];
        }
    }
}

#pragma mark - NotificationCenter methods

- (void)cancelActiveHTTPConnections 
{   
    [commentsRequest clearDelegatesAndCancel];
}

- (void) applicationWillResignActive:(NSNotification *) notification 
{
    //NSLog(@"applicationWillResignActive in DocumnetViewController");
    [self cancelActiveHTTPConnections];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    [webView stopLoading];
    [webView reload], webView = nil;
    [boldButton release], boldButton = nil;
    [italicButton release], italicButton = nil;    
    [list1Button release], list1Button = nil;
    [list2Button release], list2Button = nil;
    [fontColorButton release], fontColorButton = nil;
    [undoButton release], undoButton = nil;
    [redoButton release], redoButton = nil;
    [clearFormatButton release], clearFormatButton = nil;
    [actionSheet release], actionSheet = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{    
    [webView stopLoading];
    [webView reload], webView = nil;
    [HUD release];
    [cmisObjectId release];
    [cmisObjectId release];
    [selectedAccountUUID release];
    [tenantID release];
    [commentsRequest release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return YES;
}

- (NSString *)colorFromTag:(int)numberButton{
    switch (numberButton) {
        case 1:{
            return @"#000000";
            break;
        }
        case 2:{
            return @"#7F0000";
            break;
        }
        case 3:{
            return @"#FF0000";
            break;
        }
        case 4:{
            return @"#FF7F00";
            break;
        }
        case 5:{
            return @"#FFFF00";
            break;
        }
        case 6:{
            return @"#7FFF00";
            break;
        }
        case 7:{
            return @"#00FF00";
            break;
        }
        case 8:{
            return @"#00FF7F";
            break;
        }
        case 9:{
            return @"#00FFFF";
            break;
        }
        case 10:{
            return @"#007FFF";
            break;
        }
        case 11:{
            return @"#0000FF";
            break;
        }
        case 12:{
            return @"#7F00FF";
            break;
        }
        case 13:{
            return @"#FF00FF";
            break;
        }
        case 14:{
            return @"#FF007F";
            break;
        }
        case 15:{
            return @"#7F7F7F";
            break;
        }
        case 16:{
            return @"#FFFFFF";
            break;
        }            
        default:{
            return @"white";
            break;
        }
    }
}

@end
