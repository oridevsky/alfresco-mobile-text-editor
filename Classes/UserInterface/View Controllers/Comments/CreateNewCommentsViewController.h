//
//  CreateNewCommentsViewController.h
//  FreshDocs
//
//  Created by oleg sfd on 29.05.12.
//  Copyright (c) 2012 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeHTTPRequest.h"

@class CommentsHttpRequest;
@class MBProgressHUD;

typedef enum {
    ToolbarButtonStateUnknown = 0,
    ToolbarButtonStateOn,
    ToolbarButtonStateOff,
} ToolbarButtonState;

@interface CreateNewCommentsViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate, LikeHTTPRequestDelegate>{
    
    IBOutlet UIWebView * webView;
    IBOutlet UIToolbar * toolBar;
    IBOutlet UIView * buttonsView;
    
    NSTimer *timer;  
    
    IBOutlet UIButton * boldButton;
    IBOutlet UIButton * italicButton;    
    IBOutlet UIButton * list1Button;
    IBOutlet UIButton * list2Button;
    IBOutlet UIButton * fontColorButton;
    IBOutlet UIButton * undoButton;
    IBOutlet UIButton * redoButton;
    IBOutlet UIButton * clearFormatButton;
    
    UIActionSheet * actionSheet;
    BOOL actionSheetShown;
    
    NSString    *prevStr;
    MBProgressHUD *HUD;
    NSString *cmisObjectId;
    NSString * selectedAccountUUID;
    NSString *tenantID;
    CommentsHttpRequest *commentsRequest;
    
    IBOutlet UIView * selectColorView;
    
    ToolbarButtonState boldButtonState;
    ToolbarButtonState italicButtonState;
    ToolbarButtonState undoButtonState;
    ToolbarButtonState redoButtonState;
    
       
}

@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, readwrite, retain) MBProgressHUD *HUD;
@property (nonatomic, retain) NSString *cmisObjectId;
@property (nonatomic, retain) NSString * selectedAccountUUID;
@property (nonatomic, retain) NSString *tenantID;
@property (nonatomic, retain) CommentsHttpRequest *commentsRequest;

- (void)checkSelection:(id)sender;

- (IBAction)onBold:(id)sender;
- (IBAction)onItalic:(id)sender;
- (IBAction)onList1:(id)sender;
- (IBAction)onList2:(id)sender;
- (IBAction)onFontColor:(id)sender;
- (IBAction)onUndo:(id)sender;
- (IBAction)onRedo:(id)sender;
- (IBAction)onClearFormat:(id)sender;
- (IBAction)onSelectColorButton:(id)sender;

- (IBAction)onSaveButton:(id)sender;

- (NSString *)colorFromTag:(int)numberButton;

@end
